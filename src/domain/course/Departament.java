package domain.course;

public class Departament {
    private Integer id;
    private String name;
    private Course course;

    public Departament(Integer id, String name, Course course) {
        this.id = id;
        this.name = name;
        this.course = course;
    }
    
    public Departament(String name, Course course) {
        this.name = name;
        this.course = course;
    }
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "Departament{" +
                "name='" + name + '\'' +
                ", " + course.toString() +
                '}';
    }
}
