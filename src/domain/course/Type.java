package domain.course;

public enum Type {
    GRADUATION, MASTER, PHD;
    
    public static Type getEnumByValue(String value) {
        switch(value) {
            case "Graduação": {
                return GRADUATION;
            }
            case "Mestrado": {
                return MASTER;
            }
            case "Doutorado": {
                return PHD;
            }
            default: {
                return null;
            }
        }
    }
}
