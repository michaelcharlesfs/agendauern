package domain.booking;

import domain.room.Room;

import java.time.LocalDateTime;
import java.util.Objects;

public class Booking {
    private Integer id;
    private LocalDateTime start;
    private LocalDateTime end;
    private Status status;
    private Authorizer authorizer;
    private Requester requester;
    private Room room;

    public Booking(Integer id, LocalDateTime start, LocalDateTime end, Status status, Authorizer authorizer, Requester requester, Room room) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.status = status;
        this.authorizer = authorizer;
        this.requester = requester;
        this.room = room;
    }

    public Booking(LocalDateTime start, LocalDateTime end, Requester requester, Room room) {
        this.start = start;
        this.end = end;
        this.requester = requester;
        this.room = room;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Authorizer getAuthorizer() {
        return authorizer;
    }

    public void setAuthorizer(Authorizer authorizer) {
        this.authorizer = authorizer;
    }

    public Requester getRequester() {
        return requester;
    }

    public void setRequester(Requester requester) {
        this.requester = requester;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Booking booking = (Booking) o;
        return start.equals(booking.start) &&
                end.equals(booking.end) &&
                status == booking.status &&
                room.equals(booking.room);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end, status, room);
    }
}
