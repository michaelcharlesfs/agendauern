package domain.booking;

public interface Authorizer {
    void approveBooking(Booking booking);
    void disapproveBooking(Booking booking);
}
