package domain.booking;

import domain.room.Room;

import java.time.LocalDateTime;

public interface Requester {
    void requestBooking(Room room, LocalDateTime start, LocalDateTime end);
}
