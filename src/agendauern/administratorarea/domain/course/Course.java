package domain.course;

public class Course {
    private String name;
    private Type type;
    private College college;

    public Course(String name, Type type, College college) {
        this.name = name;
        this.type = type;
        this.college = college;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }

    @Override
    public String toString() {
        return "Course{" +
                "nameCourse='" + name + '\'' +
                ", typeCourse=" + type +
                ", " + college.toString() +
                '}';
    }
}
