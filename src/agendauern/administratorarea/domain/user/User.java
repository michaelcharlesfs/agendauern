package domain.user;

import java.util.Objects;

public class User {
    Username username;
    Password password;
    Status status;

    User(Username username, Password password, Status status) {
        this.username = username;
        this.password = password;
        this.status = status;
    }

    User(Username username, Password password) {
        this.username = username;
        this.password = password;
        this.status = Status.ACTIVE;
    }

    public Username getUsername() {
        return username;
    }

    public Password getPassword() {
        return password;
    }

    public Status getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return username.equals(user.username) &&
                password.equals(user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }
}
