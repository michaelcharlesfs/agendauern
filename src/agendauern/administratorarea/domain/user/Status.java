package domain.user;

public enum Status {
    ACTIVE, INACTIVE
}
