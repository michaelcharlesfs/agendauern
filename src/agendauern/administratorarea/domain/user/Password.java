package domain.user;

import java.util.Objects;

public class Password {
    private String value;

    Password(String value) {
        this.value = this.encryptPassword(value);
    }

    public String getPassword() {
        return this.decryptPassword();
    }

    private String decryptPassword() {
        return this.value;
    }

    private String encryptPassword(String password) {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Password password = (Password) o;
        return value.equals(password.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
