package domain.user;

import java.util.Objects;

public class Username {
    private String value;

    Username(String value) {
        this.value = value;
    }

    public String getUsername() {
        return this.value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Username username = (Username) o;
        return value.equals(username.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
