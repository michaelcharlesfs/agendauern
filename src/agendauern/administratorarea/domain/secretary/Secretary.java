package domain.secretary;

import domain.booking.Authorizer;
import domain.booking.Booking;
import domain.course.Departament;
import domain.util.Person;

public class Secretary extends Person implements Authorizer {
    private Integer id;
    private Departament departament;

    public Secretary(Integer id, Departament departament, String name) {
        this.id = id;
        this.departament = departament;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Departament getDepartament() {
        return departament;
    }

    public void setDepartament(Departament departament) {
        this.departament = departament;
    }

    @Override
    public void approveBooking(Booking booking) {

    }

    @Override
    public void disapproveBooking(Booking booking) {

    }
}
