package domain.professor;

import domain.booking.Requester;
import domain.course.Departament;
import domain.room.Room;
import domain.user.User;
import domain.util.Person;

import java.time.LocalDateTime;

public class Professor extends Person implements Requester {
    private Integer id;
    private Departament departament;
    private User user;

    public Professor(Integer id, String name, Departament departament) {
        this.id = id;
        this.name = name;
        this.departament = departament;
    }

    public Professor(Integer id, String name, Departament departament, User user) {
        this.id = id;
        this.name = name;
        this.departament = departament;
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Departament getDepartament() {
        return departament;
    }

    public void setDepartament(Departament departament) {
        this.departament = departament;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public void requestBooking(Room room, LocalDateTime start, LocalDateTime end) {

    }
}
