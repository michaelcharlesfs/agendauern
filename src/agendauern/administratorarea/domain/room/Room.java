package domain.room;

import domain.course.Departament;

public class Room {
    private Integer id;
    private String name;
    private Departament departament;

    public Room(Integer id, String name, Departament departament) {
        this.id = id;
        this.name = name;
        this.departament = departament;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Departament getDepartament() {
        return departament;
    }

    public void setDepartament(Departament departament) {
        this.departament = departament;
    }
}
