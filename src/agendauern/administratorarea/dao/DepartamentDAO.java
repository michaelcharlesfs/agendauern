package agendauern.administratorarea.dao;

import agendauern.config.Database;
import domain.course.College;
import domain.course.Course;
import domain.course.Departament;
import domain.course.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DepartamentDAO {
    
    Connection connection = new Database().getConnection();
    
    public DepartamentDAO() {
        
    }
    
    public void add(Departament departament) {
        try {
            String query = "INSERT INTO public.departaments(" +
        "            college_name, name, course_name, course_type)" +
        "    VALUES (?, ?, ?, ?)";
            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setString(1, departament.getCourse().getCollege().getName());
            ps.setString(2, departament.getName());
            ps.setString(3, departament.getCourse().getName());
            ps.setInt(4, departament.getCourse().getType().ordinal());
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(DepartamentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Departament> getAll() {
        try {
            String query = "select * from departaments";
            ResultSet resultSet;
            resultSet = this.connection
                    .prepareStatement(query)
                    .executeQuery();
            List<Departament> result = new ArrayList<>();
            while(resultSet.next()) {
                College college = new College(resultSet.getString("college_name"));
                Course course = new Course(resultSet.getString("course_name"), 
                        Type.values()[resultSet.getInt("course_type")], 
                        college);
                Departament departament = new Departament(resultSet.getInt("id"),
                        resultSet.getString("name"), 
                        course);
                result.add(departament);
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(DepartamentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<>();
    }
    
}
