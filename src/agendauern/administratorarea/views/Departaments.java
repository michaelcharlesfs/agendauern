/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendauern.administratorarea.views;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "departaments", catalog = "agendauern", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Departaments.findAll", query = "SELECT d FROM Departaments d"),
    @NamedQuery(name = "Departaments.findById", query = "SELECT d FROM Departaments d WHERE d.id = :id"),
    @NamedQuery(name = "Departaments.findByCollegeName", query = "SELECT d FROM Departaments d WHERE d.collegeName = :collegeName"),
    @NamedQuery(name = "Departaments.findByName", query = "SELECT d FROM Departaments d WHERE d.name = :name"),
    @NamedQuery(name = "Departaments.findByCourseName", query = "SELECT d FROM Departaments d WHERE d.courseName = :courseName"),
    @NamedQuery(name = "Departaments.findByCourseType", query = "SELECT d FROM Departaments d WHERE d.courseType = :courseType")})
public class Departaments implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "college_name")
    private String collegeName;
    @Column(name = "name")
    private String name;
    @Column(name = "course_name")
    private String courseName;
    @Column(name = "course_type")
    private Integer courseType;

    public Departaments() {
    }

    public Departaments(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        Integer oldId = this.id;
        this.id = id;
        changeSupport.firePropertyChange("id", oldId, id);
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        String oldCollegeName = this.collegeName;
        this.collegeName = collegeName;
        changeSupport.firePropertyChange("collegeName", oldCollegeName, collegeName);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldName = this.name;
        this.name = name;
        changeSupport.firePropertyChange("name", oldName, name);
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        String oldCourseName = this.courseName;
        this.courseName = courseName;
        changeSupport.firePropertyChange("courseName", oldCourseName, courseName);
    }

    public Integer getCourseType() {
        return courseType;
    }

    public void setCourseType(Integer courseType) {
        Integer oldCourseType = this.courseType;
        this.courseType = courseType;
        changeSupport.firePropertyChange("courseType", oldCourseType, courseType);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Departaments)) {
            return false;
        }
        Departaments other = (Departaments) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "agendauern.administratorarea.views.Departaments[ id=" + id + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
