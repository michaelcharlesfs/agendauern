package agendauern.config;

import java.awt.Component;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import javax.swing.JOptionPane;

public class Database {
    private Connection connection;
    private final String connectionURL;
    private final String user;
    private final String password;
    private final String dataBaseDriver = "org.postgresql.Driver";
    
    public Database() {
        connection = null;
        connectionURL = "jdbc:postgresql://localhost/agendauern";
        user = "agenda";
        password = "agenda";
        
        try {
            Class.forName(dataBaseDriver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    public Connection getConnection() {
        try {
            connection = DriverManager.getConnection(connectionURL, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
